@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <!--
            <div class="card card-success">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-6 col-xl-12">
                            <div class="card mb-2 bg-gradient-dark">
                                <img class="card-img-top" style="opacity:0.4!important;" src="{{asset('dist/img/assembleia-dash.jpg')}}" alt="Assembleia Nacional">
                                <div class="card-img-overlay d-flex flex-column justify-content-end">
                                    <h5 class="card-title text-primary text-white">Bem vindo!</h5>
                                    <p class="card-text text-white pb-2 pt-1">Este é o sistema de comunicação de dados para o MINFIN.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            -->
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Dashboard</h1>
                    {{$_SERVER['HTTP_HOST']}}
                    <br>{{json_encode($_SERVER)}}
                </div><!-- /.col -->
            </div><!-- /.row -->
            <div class="row">
                @if(Auth::user()->isAdmin())
                    @livewire('dashboard.user-widget')
                    @livewire('dashboard.user-widget')
                    @livewire('dashboard.user-widget')
                    @livewire('dashboard.user-widget')
                @endif
            </div>
        </div><!-- /.container-fluid -->
    </div>

@endsection

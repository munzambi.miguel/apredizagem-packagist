<?php

namespace App\Http\Livewire\Comuns;

use Livewire\Component;

class HeaderSearch extends Component
{
    public function render()
    {
        return view('livewire.comuns.header-search');
    }
}

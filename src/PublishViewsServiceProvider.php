<?php
//  "Kinsari\\Aprendizagem\\": "src/"
namespace Kinsari\Aprendizagem;

use Illuminate\Support\ServiceProvider;

class PublishViewsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->publishes([
            __DIR__ . '/Http' => app_path('Http'),
            __DIR__ . '/views' => resource_path('views')
        ], 'send-views');
    }
}
